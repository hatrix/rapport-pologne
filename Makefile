SHELL=/bin/bash

build:
	pandoc report.md -o report.tex --number-sections -H options.sty --template my.latex

	sed -i 's/begin{figure}\[htbp\]/begin{figure}\[H\]/g' report.tex

	pdflatex report.tex
	pdflatex report.tex
	pdflatex report.tex

	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH \
	-sOutputFile=report_comp.pdf report.pdf
	mv report_comp.pdf report.pdf

clean:
	rm -f report.{aux,log,glo,gls,ist,lof}
	rm -f report.{tex,toc,out}
	rm -f report.pdf

﻿\newpage

# Préface

Étant donné que peu de personnes partent en Pologne et que donc peu
d'informations sont disponibles, j'me suis dit que j'allais faire un petit
guide pour ce rapport.

J'étais en effet seul à partir mon année, promo 2018 et une ou deux personnes 
en 2017, pas des masses. Mon séjour s'est déroulé de mars à juillet 2015. Vous
aurez donc à faire votre stage en janvier - février, durée minimale de 6 
semaines. Ce qui vous laisse en fait la moitié de juillet et tout août pour
vos vacances !

![](images/preface.jpg)

![](images/preface2.jpg)

# Pourquoi partir en Pologne ?

## Généralités

Plein de raisons. Vraiment plein.
Ci-dessous une énumération non exhaustive, certains points seront de toute
façon repris plus tard.

* la bière (pas chère)
* la vodka (pas chère)
* les bars (pas chers)
* les connexions entre villes (pas chères)
* les filles (…)
* au final rien n'est cher
* les paysages
* les villes très jolies
* l'amabilité des gens


## Plus spécifiquement

Bon, on en doute pas, j'ai adoré mon semestre à l'étranger. Du coup, qu'est-ce
qui m'a motivé à partir en Pologne, cet ex pays communiste de l'Est paumé
dont on entend jamais parler ?

Eh beh en fait… j'ai choisi un peu au pif, on va pas se mentir. En gros, je
n'avais pas envie de passer un semestre avec une quinzaine d'autres épitéens.
Si on part à l'étranger, c'est pas pour parler français. Mais c'est aussi pour
se démerder par soi-même.
Et puis, la Pologne, j'y connaissais rien, à part les préjugés que tout le
monde a. J'avais envie de changer ça, de voir ce que c'était vraiment, de
bouger un peu quoi.

Au final je suis très content de mon choix. Aussi, pensez bien que c'est une
destination Erasmus. Ce qui veut dire que vous aurez environ 220€ par mois en
tant que bourse.
Payer ou se faire payer pour étudier, à vous de voir !

Déjà que la vie n'est pas chère du tout là-bas, si en plus on reçoit une
bourse… C'est pas mal quoi. 

N'hésitez pas à faire un tour sur mon
blog\footnote{\url{http://pologne.hatrix.fr}} ou encore à aller voir les photos
de mes voyages\footnote{\url{http://pologne.hatrix.fr/images/}}.

Si vous décidez donc d'aller en Pologne, vous débarquerez à Cracovie (Kraków),
l'ancienne capitale du pays. Une ville magnifique dont voici le centre, plus
grande place médiévale d'Europe :

![Rynek de Cracovie](images/cracovie.jpg)

\newpage

# Avant de partir

## Spécificités de la Pologne

La Pologne a beau être dans la zone Euro, elle n'en a pas la monnaie. D'après
ce que j'ai compris, elle est obligée d'adopter l'Euro vu ce qui a été signé.
Mais en gros, ils s'en fichent et ils estiment que c'est pas une bonne idée.
Du coup, la monnaie utilisée est le złoty (prononcer zwoté), presque pile 4 zł 
pour 1€, pratique. En revenant de votre séjour vous connaîtrez bien votre table
de 4.

Traversez seulement là où il y a des passages piétons, sinon vous pouvez vous
choper une amende. Aussi, ne traversez qu'au vert, les gens sont disciplinés
et vous risquez une amende en plus de passer pour un con.

## Logement

Il est possible d'obtenir des logements à l'université. Mais vu la réactivité
des différents services il est préférable de chercher un appartement de votre
côté.
On peut s'en sortir facilement pour 1200 zł (300€) pour un 35m² seul à 20
minutes de l'université à pied.

Je suis personnellement passé par une agence, en anglais, via internet. Ce qui
aussi explique le prix de 1800 zł avec charges, mais en plein centre. Mon
appartement était en effet situé à 5 minutes de l'école, 5 minutes de la gare
principale avec son grand centre commercial et à 15 minutes du centre même de
la ville avec ses bars, etc.

Ça reste moins cher qu'à Villejuif, pour quelque chose de mieux situé, de plus
grand et de mieux meublé. Et puis on oublie encore pas, on a une bourse.

## Transport

Pour aller en Pologne, c'est pas compliqué, pas beaucoup d'options :

- en voiture
- en avion (EasyJet, Ryanair, …)
- en bus (Ecolines, Eurolines)

En avion on peut s'en sortir facilement avec 120€ l'aller plus deux valises de
20kg chacune.
Je suis parti avec EasyJet de Basel en Suisse vu que j'habite en Alsace. Des
vols RyanAir ne sont vraiment pas cher à départ de Paris par contre !

## Programme de mentor

Étant donné qu'on est en Erasmus, on a droit à des mentor. Un mentor, ou même
des fois une si vous avez de la chance, c'est quelqu'un, censé avoir les mêmes
goûts que vous, qui vous aidera lors de votre arrivée à Cracovie.

Certains mentors sont plus gentils que d'autres, c'est normal après tout, ils
ne sont que volontaires et ne savent pas sur qui ils vont tomber.
Le mien m'a donc cherché à l'aéroport, on a fait quelques parties d'airsoft
ensemble ainsi que du wakeboard. Sans compter tous les petits conseils ou
réponses à mes questions.

## Facebook

Il existe un groupe
Facebook\footnote{\url{https://www.facebook.com/groups/300965885686/}} des
français et francophones de Cracovie. Endroit idéal pour suivre l'actu en
rapport avec la ville et la Pologne. Mais aussi pour avoir les bons plans et
les comprendre !

\newpage

# Arrivée

## Trucs utiles

Commencez par télécharger l'application _jakdojade_ sur votre téléphone.
Littéralement « comment se rendre », cette application vous permet d'avoir les
temps d'attente ainsi que les différentes lignes à prendre pour vous rendre où
vous voulez.
Ça marche mieux que celle de la RATP, et, gros bonus, toutes les villes de
Pologne utilisent ce système.

## Aéroport

L'aéroport est à 40 minutes en bus du centre ville, utilisez jakdojade.
Si vous avez un mentor, vous pouvez lui demander de vous emmener. C'est pas
facile quand on connait pas !

## Où acheter des trucs

On n'est pas trop dépaysé en Pologne niveau magasins. Vous trouverez facilement
un Carrefour ou un Auchan. Si non, Żabka et Biedronka sont plus communs, de
petits magasins de proximité.

Après pour faire du shopping il y a plusieurs grandes galeries commerciales,
une bonne demi-douzaine à pas 20 minutes du centre en tram.
Souvent avec un cinéma, la plus grande franchise étant Cinema City. Environ 4€
la place avec tarif étudiant !

\newpage

# Vie étudiante
## Erasmus

Erasmus, c'est bien. Vous êtes dans une université où il y a des activités, où
des gens organisent des trucs.
En plus du chargé Erasmus de l'université, vous avez votre coordinateur, votre
mentor mais aussi le bureau Erasmus étudiant. Ce sont des élèves, volontaires,
qui organisent des trucs. En résumé : ils kiffent la vibe gratos avec les leurs
potes et les étrangers.

Déjà à votre arrivée, il y a la semaine d'orientation. Une semaine où il n'y
a pas cours, la seule chose qu'il y a à faire c'est aller aux soirées. Mon
année, j'ai eu ça :

- Speed Friending
- Welcome to Poland party, un byob
- Laser Tag
- Pub Crawl
- Schindler's Factory
- Wedding Party
- City Games, un genre de chasse au trésor
- Tram Party
- Trip à Zakopane pour finir

Après ça, ça se calme un peu, mais il y a tout de même des événements par-ci
par-là !
Comme un voyage à Prague, à Auschwitz, à Wieliczka ou encore du wakeboard.

\newpage

## Soirées

### Tandem

![](images/tandem.jpg)

Des soirées Tandem sont organisées tous les lundis soir au club B4, près du
centre. À voir si ça restera ce jour là, mais c'est régulier.

Ce sont des soirées où vous pouvez échanger dans la langue que vous voulez, des
drapeaux sont accrochés au dessus des tables. Parfait pour rencontrer du monde
intéressant autour d'une bière !\
Les drapeaux ne sont pas toujours respectés mais on trouve quand même assez
facilement des français. C'est donc un bon moyen pour choper des conseils et de
se faire une rapide idée de la Pologne lorsque vous arrivez.

Il y a aussi un groupe
facebook\footnote{\url{https://www.facebook.com/groups/TANDEMKRAKOW}} où vous
pouvez mettre un petit message du genre « I'm a native french and I'm looking
for someone to speak polish with, I'm a beginner ». Rien que ça, ça vous
permettra de pas trop galérer. Et si vous voulez vous perfectionner en anglais,
y'a aussi du monde qui cherche. J'ai rencontré des gens géniaux juste en
répondant à un message parce que je m'emmerdais un après-midi !

Et puis, vous, vous restez pour 5 mois. Certains sont là depuis plusieurs
années ! C'est pratique pour choper les bonnes adresses, surtout niveau bars et
boîtes.

\newpage

### Tram Party

![](images/tram_party.jpg)

Je pense que le nom explique pas trop mal le concept. Si jamais : un tram est
loué, il se balade sur les lignes en ville et vous, vous buvez dedans avec
de la musique.

Il y en a des régulièrement d'organisés. C'est genre 5€ « l'entrée ».

\newpage

# Campus
## Université

L'université où vous serez envoyé, c'est Politechnika Krakowska. À prononcer
« Politernika Krakofska », petit piège pour faire chier le monde.
Il y a plusieurs bâtiments : Électronique et Informatique, Chimie, 
Architecture, Bibliothèque, Génie civil et quelques autres. Vous, vous serez
dans la branche informatique du bâtiment de Chimie, oui.

La direction Erasmus se situe dans le bâtiment principal, au premier à droite,
assez facile à trouver. Par contre ils ne bossent que de 9h à 12h.

## Locaux

Les locaux, c'est simple : j'ai pas dû voir plus de 4 salles. Les classes sont
assez petites vu qu'on est qu'entre Erasmus, on atteint difficilement les 10
personnes. Ça m'est déjà arrivé d'être seul en cours !
Tout ça pour dire que de petites salles, ça suffit. Une salle avec projecteur
ou une salle informatique et c'est tout bon. C'est pas vieux mais c'est pas
récent, c'est correct en somme.

## Cours

Vous devez prendre 5 cours, chacun donnant 6 crédits ECTS pour au final en
valider 30. Le cours de polonais donne 5 ECTS, faut donc pas trop compter
dessus si vous en ratez un autre !
Bien évidemment, je ne vais parler en détails que de ceux que j'ai pris.

Alors d'abord, bah c'est en anglais, encore heureux. Les cours sont un peu bof
à mon goût mais ceux de C et C++ rattrapent pas mal. Sur mes 5 cours, il n'y en
a eu qu'un avec un examen, les autres étaient juste en contrôle continu avec
des projets à rendre.

- High Performance Computing : un cours sur les performances, ça part d'un
historique des supercalculateurs pour faire du hardware avec notamment les 
registres ou pipelines d'un CPU pour finir sur les optimisations du code. Cours
un poil compliqué si on ne sait pas coder mais a priori avec un projet en C au
S3 on s'en sort.
- Neural Networks : cours assez chiant. En gros vous faites des présentations
sur les réseaux de neuronnes, la prof ne sert à rien.
- Numerical Methods : quelques projets à rendre dans le langage que vous voulez
sur un exercice en rapport avec les maths. Par exemple coder plusieurs méthodes
pour déterminer f(x) = 0, des conneries comme ça. Assez facile et ne prend que
45 minutes par semaine.
- Discrete Mathematics : un cours sur les graphes. Première fois cette année
donc ça risque de changer la prochaine. On a eu à coder l'implem de graphes
donnés avec matrice d'adjacence pour faire du Kruskal et du Prim dessus.
- C++ Programming : le meilleur cours ! Le prof est cool et part de la base de 
chez base. Les exercices sont intéressants et on arrive aux classes et 
templates. A priori on doit coder sur Visual Studio, mais il ne râle pas sur
GCC.

Vous pouvez choper les cours de C++\footnote{\url{http://www.michalbereta.pl/}}
et de High Performance
Computing\footnote{\url{http://riad.pk.edu.pl/~fkruzel/laboratoryHPC.html}} en
ligne. À savoir combien de temps ça va rester up.

\newpage

# Vivre en Pologne

## Cracovie

On pourrait passer du temps sur Cracovie tellement il y a de choses à
raconter ! À moins de vous balader dans Nowa Huta, le quartier industriel, vous
allez tout le temps trouver des choses intéressantes.

Il n'est pas rare de tomber sur une église énorme en se trompant juste d'une
rue. Et puis même, rien que les bâtiments eux-mêmes… J'avais vue sur une église
dans la rue principale de Cracovie, vraiment génial !

### Stare Miasto

Stare Miasto : Vieille ville. Donc logiquement tout ce qui est mignon tout
plein au centre. Gagné, c'est ça.

L'entrée de la ville, à l'époque, elle se faisait via la Barbakan, un fort
posté devant les portes de la ville. Maintenant, il ne reste plus que la tour
de ladite porte avec cette fortification. Pas dégueu quand vous devez passer
devant pour aller au bar.

![](images/barbakan.jpg)


La place du marché, Rynek, c'est tout simplement la plus grande place médiévale
d'Europe.
Bon, y'a un bâtiment en plein milieu, mais on peut le traverser donc ça compte
pas vraiment, ça reste énorme.

![](images/stare_miasto.jpg)

C'est sur cette place qu'on retrouve énormément d'événements : fête de la
musique, fête de la constitution, concerts, marathon, cyclisme, etc. Et tout
autour, vous avez les bars, les boîtes, les restaurants et… les strip clubs.
Les strip clubs, y'en a des tonnes. Et des nanas en font la pub dans la rue
sans aucun problème. C'est toujours marrant, on se fait aborder à coup sûr
quand on se balade avec des mecs, mais jamais avec des filles. Vous étonnez
donc pas si on vient vous parler dans la rue, surtout sur Floriańska.


### Kazimierz

C'est ici l'ancien quartier juif. C'est assez délabré, mais quand même joli. On
y trouve plein de bars, des magasins d'antiquités mais aussi des artistes qui
vendent dans la rue. Bien entendu plusieurs églises et également de grandes
fresques sur les murs de certains bâtiments.

![Plac Nowy](images/kazimierz.jpg)

C'est ici le centre de Kazimierz, à comprendre : des bars et de la bouffe. Le
bâtiment central de la place renferme plusieurs magasins. C'est là qu'on
achète, selon moi hein, les meilleures zapiekanka de la ville !

\newpage

## La bière

La bière en Pologne, c'est surtout des pils. Mais comme ils ne font que ça, on
se retrouve avec des bières pas _trop_ mauvaises.
C'est pas l'extase, mais ça reste correct, surtout encore vu les prix : 4 zł et
des fois moins en magasin pour une canette de 50cl. Dans les 6-8 zł en bar ou
restaurant, pour une pression en 50cl.

Petite liste : 
Tyskie\footnote{\url{http://www.ratebeer.com/beer/tyskie-gronie/4989/}},
Żywiec\footnote{\url{http://www.ratebeer.com/beer/zywiec-jasne-pelne--beer/6558/}},
Okocim\footnote{\url{http://www.ratebeer.com/beer/okocim-jasne-pelne--beer/17915/}},
Perła\footnote{\url{http://www.ratebeer.com/beer/per322a-chmielowa-hop-pearl/10401/}}.

Voici pour les grandes marques que l'on retrouve en bar et restaurant. En
supermarché vous trouvez beaucoup de Ciechan, Warka, Łomża.

Ce que vous retrouvez beaucoup en Pologne, comme en Allemagne en fait, ce sont
les Radler : un genre de panaché mais avec plus de bière que de limonade dans
le ratio. Également les bières au fruit de Redd's.

## Les prix

J'arrête pas de dire que c'est pas cher, mais à quel point ? Je m'emmerdais
un jour donc je suis allé à Carrefour et j'ai fait une petite 
liste des prix\footnote{\url{http://pologne.hatrix.fr/liste/}} de trucs 
intéressants. Surtout niveau boissons en fait.

Carrefour, c'est bien mais on peut encore trouver moins cher. Il y a aussi le
magasin Alma, mais lui, il est cher. C'est un peu le luxe de l'alimentaire.
Vous avez envie de payer 4€ les 125g de munster ? Moi pas.

\newpage

## Événements
### Craft Beer Week

![Browar Dukla](images/festival.jpg)

Première édition de ce festival de la bière, tenu du 6 au 8 mars. Ils vont le
refaire je suppose, ça a bien marché ! C'est cool comme évenement pour
commencer son semestre.

Une petite quinzaine de micro-brasseries des environs, y'en aura pour tout le
monde. Comme d'habitude dans ce genre d'événement, beaucoup d'IPAs mais si on
fouille un peu on peut trouver des trucs biens.

C'est l'occasion de parler aux gens, de découvrir ce que la Pologne aime et
aussi de se rendre compte que des français, y'en a pas beaucoup au festoche.

\newpage

### Rękawka

![Fête païenne](images/rekawka.jpg)

Le Rękawka, c'est une fête traditionnelle qui se passe après Pâques. En 2015,
c'était le 7 avril. Je vais pas vous parler de l'histoire du truc, je la
connais pas et j'ai encore du mal à lire les articles en polak…

Ce qu'il faut retenir : c'est un rassemblement au Kopiec
Krakusa\footnote{\url{https://fr.wikipedia.org/wiki/Tumulus_de_Krakus}} — qui
serait la tombe du roi Krakus — assez populaire. On y retrouve des batailles,
de la bouffe, des ateliers de fabrication de boucliers, d'épées, de bijoux et
d'un tas d'autres choses.

L'entrée est bien évidemment gratuite, tout ce qu'il faut faire c'est trouver
une place sur le Kopiec pour regarder ce qu'il se passe.

On pourrait se dire que c'est chiant comme événement, qu'il n'y a que les vieux
que ça intéresse, mais pas du tout ! La moyenne d'âge est vraiment jeune, on
doit taper dans les 25-30 à tout casser. Et puis, faut l'admettre, les costumes
traditionnels féminins sont assez mignons !

\newpage

### Wielka Parada Smoków

![Parade des dragons](images/smokow.jpg)

Si vous n'y allez pas, honte à vous. C'est un des plus beaux et plus grands
feux d'artifices que j'ai pu voir ! Le spectacle dure dans les 50 minutes, son,
lumières et explosifs à ne plus savoir qu'en faire ! À côté de ça le 14 juillet
c'est un peu bof…

Plusieurs bâteaux se balladent sur le fleuve avec des dragons gonflés au dessus
d'eux, ils simulent des batailles. C'est très bien géré et c'est plutôt
plaisant. Faut juste prévoir d'arriver assez tôt, il y avait d'après eux dans
les 80 000 personnes ce soir là ! Rien que sur Facebook il y en avait plus de
20 000.

En 2015, ça s'est donc passé le 30 mai. Aucune idée si c'est à date fixe ou
non. Et puis c'est juste à côté du château Wawel, un cadre parfait !

\newpage

### Krakowski Festival Piwa

![Festival en plein air](images/festival2.jpg)

Festoche un peu plus grand, tenu les 6 et 7 juin. Cette fois, non pas en
intérieur mais en extérieur ! Ça fait plaisir de boire au soleil.
Les différents stands vendaient de la bière mais aussi à manger, des
spécialités du coin ou non d'ailleurs.

J'ai passé bien 1h à parler avec un brasseur venant de Katowice. C'était leur
première bière et elle était franchement pas mauvaise : 67 sur ratebeer. Leur
seconde est à
88\footnote{\url{http://www.ratebeer.com/beer/hajer-flaps/343690/}}, ça me
donne envie d'y retourner !

\newpage

### Wianki

![Encore une fête d'origine païenne !](images/wianki.jpg)

Je vais pas ressortir du wikipédia, ça va bien un moment mais faut savoir
s'arrêter. Alors le Wianki, en gros, c'est une fête traditionnelle qui coïncide
de nos jours avec la fête de musique. Et donc ça se passe le 21 juin.

On peut y voir des feux d'artifices et scènes sur le fleuve près de Wawel. Mais
également partout en ville. Deux scènes sont présentes sur la place principale
et une autre plus petite sur la place d'à côté.

Y'a du monde, y'a beaucoup de musique. Plein de scènes partout partout. Encore
un événement grandiose et gratos !

\newpage

## Apprendre le polonais

Ça peut paraître dur au premier abord, mais j'vous jure, c'est pas si compliqué
que ça ! Et puis ça fait toujours plaisir de comprendre ce qui est écrit autour
de vous, ou ce qu'on vous raconte. Parce que oui, les gens parlent pas anglais,
à part les jeunes, et encore, pas toujours !

Les vieux sont super heureux quand on arrive à aligner 4 mots en polonais. Une
fois un SDF m'a même fait une petite danse, c'est vous dire. Ils doivent être
contents que quelqu'un essaye d'apprendre leur langue, pas courant.

Du coup, comment on apprend le polak ? Avec les cours de l'université vous
arriverez pas super loin. C'est très scolaire. Genre savoir dire l'heure, ça
sert vraiment dans la vraie vie ? On dirait "five hour and twenty minute" en
anglais que c'est compréhensible.

Perso, j'utilise Memrise\footnote{\url{http://memrise.com/}}. Un site un peu
comme le Projet Voltaire. Si on se trompe, on se retape les mots. Pratique pour
apprendre du vocabulaire, pas forcément la grammaire. Pour ça, vous avez Tandem
ou encore le livre "Hurra!!! Po Polsku", bon bouquin !

## Spécialités culinaires

Sans être exhaustif, voici quelques spécialités qu'il vous faudra tester une
fois rendu en Pologne. N'hésitez pas à aller au restaurant, on paye rarement
plus de 10€ son repas, boisson comprise.
Le mieux reste quand vous invitez une fille et qu'au final vous ne payez que
12€ avec les bières. Comme ça, même si ça mène nulle part, vous n'avez rien
perdu !

\newpage

### Pierogi

![Pierogi](images/pierogi.jpg)

Appelés « dumplings » sur les cartes en anglais, ce sont tout simplement un
genre de ravioles.
On en trouve pour tous les goûts : viande, champignons, choucroute, pomme de
terre et fromage…

Vraiment pas mauvais, mais surtout pas cher : aux environs de 20 zł (5€) en 
général. Souvent servis au nombre de 8, vous êtes sûr d'être calé après la fin
de votre repas !
Je ne sais toujours pas comment une amie a fait pour en manger 15 plus son
litre de bière…

\newpage

### Zapiekanka

![Zapienkanka](images/zapiekanka.jpg)

Le plus simple serait de dire que c'est un genre de pizza baguette. Niveau
taille, on tape en général dans quelque chose de plus long que votre avant
bras. Et niveau prix ? Pas plus de 10 zł (2,5€) !

On en trouve à tout : poulet, fromage, épinards, etc. Les meilleures se
trouvent sur la place de Kazimierz dans les petits magasins au centre. Vous
pouvez passer votre chemin en en voyant dans les différents kebabs, c'est
souvent de qualité médiocre. Lesdits kebabs n'arrivent d'ailleurs pas à la
cheville d'une bonne zapienkanka (et oui, Bruce) !

\newpage

### Obwarzanek

![Obwarzanki au sel et pavot](images/obwarzanek.jpg)

Spécialités des environs de Cracovie, c'est en fait un genre de Bretzel. Moins
salé, peut-être un peu plus mou, vous en trouverez partout en ville. Genre,
partout. Il y a des petits stands bleus qui en vendent tous les 200m, 
facilement.

Ça coûte en général entre 1,50 zł et 1,80 zł (moins de 0,5€) selon où vous les
achetez et ce que vous choisissez. On en trouve plutôt au pavot, sésame ou
fromage.

Un obwarzanek, ça a le mérite de ne pas coûter grand chose et de caler. Le
nombre de fois où j'ai voulu me faire un repas, que j'en ai mangé deux avant et
que j'avais plus envie de cuisiner…

\newpage

# Bon plans
## Restaurants

- Introligatornia Smaku : situé à Kazimierz, petit restau semi gastronomique. À
comprendre bon, bien présenté et pas vraiment cher.

- U Babci Maliny : un petit restau en sous-sol décoré style montagnard. On y
mange bien pour 7€, bière comprise. Il faut commander au comptoir et venir
retirer son plat à l'annonce de son numéro. 

## Bars

Vous trouverez de la bière à 6-8 zł (2€) la pinte pratiquement partout, que ça
soit dans un bar paumé comme dans le restaurant en plein sur la place du marché
au centre.

- Pijalnia : Si vous voulez encore moins cher, c'est 1€ le shot mais également
1€ les 40cl de bière.
* Viva la Pinta : 15 bières en pression, plusieurs brassées par le bar mais
aussi quelques micro-brasseries des alentours. Les prix tournent autour des 12
zł (3€) la pinte. Possibilité également de manger de bons hamburgers sur leur
grande terrasse en cour intérieure.
* Strefa Piwa : "seulement" 12 becs, similaire à Viva la Pinta en plus petit.
* TEA Time : Ils brassent également ! Tout y est de style anglais :
fish and chips, décoration, football à la télé et même leur style de bière.
- Alchemia : bar à l'ambiance "Kazimierz" : délabré, faible éclairage à la
bougie par endroits. À tester, bonne ambiance, sur la place principale du
quartier.

\newpage

## Louer une moto

![Lac à la frontière Slovaque](images/moto.jpg)

Si vous avez le permis moto, c'est top. En général quand on veut louer un
véhicule, on vous demande depuis combien de temps vous avez le permis, quel âge
vous avez, une caution, etc. Ici, on s'en fout totalement !

J'ai donc loué deux jours une GS500 pour seulement 40€, sans caution et
équipement compris ! Seule règle : partir et rentrer avec le plein.

Avec ça, vous partez un peu où vous voulez. Je suis allé à Ogrodzieniec,
Częstochowa, Żywiec, Zakopane et j'ai aussi fait un petit tour en Slovaquie.
Plusieurs lacs de barrage sur le chemin, de beaux paysages, de hautes 
montagnes, vous voulez quoi de plus ?

L'essence ne coûte pas si moins chère que ça, environ 1,20€ le SP98. On s'en
sort bien !

\newpage

## Endroits sympas

### Zakrzówek

![Lac de Zakrzówek](images/zakrzowek.jpg)

À prononcer « zakjouvek », c'est en fait une ancienne carrière, remplie
maintenant de flotte. Un endroit sympa pour aller se baigner étant donné que ce
lac est à même pas 20 minutes du centre-ville !

Vraiment dépaysant, les gens y vont pour plonger, se baigner, faire de 
l'escalade, des barbecues, boire de la bière… Les falaises atteignent 
facilement les 30 mètres.

\newpage

### Filharmonia

![Scène du Filharmonia](images/filharmonia.jpg)

J'ai pu assister à deux représentations. La première, j'ai été invité par mon
coordinateur Erasmus quand je lui ai rendu un papier, il m'a juste dit « eh au
fait y'a ça vendredi soir, tu veux ? Tu peux prendre autant de places que tu
veux — ah ok ».

La seconde, via le groupe FaceBook des français à Cracovie. Une nana avait
invité tout le monde, c'était gratuit. Restez à l'affût, c'est génial comme
soirée à passer !

\newpage

# Voyager

## Introduction

Avec seulement 2 à 3 jours de cours par semaine, vous avez franchement le temps
de voyager. Vous aurez forcément un week-end de 3 jours, voire 4 comme moi.
Avec ça, vous pouvez aller loin, pour pas cher, voir du pays.

C'est une très bonne opportunité qu'il faut saisir, pas juste se dire « je
ferais ça plus tard, là j'ai cours ». Vous _avez_ le temps, utilisez le !
Même si ce n'est que prendre un vélo pour aller voir les alentours, c'est déjà
ça. Mais franchement, avec le prix ridicule des bus, faut pas abuser…

Je vais essayer de donner justement les prix des transports de mes voyages,
pour donner une idée d'à quel point la SNCF c'est honteux.

## Erasmus

Erasmus, c'est vraiment cool. J'ai eu l'occasion de visiter plusieurs villes
très intéressantes pour vraiment pas cher.
Certaines destinations provenaient directement du groupe Erasmus de l'école,
d'autres d'un groupe facebook un peu
chelou\footnote{\url{https://www.facebook.com/erasmus.travel/}} mais qui marche
bien.

Vous avez tout, ou presque, de compris. C'est à dire le transport, l'auberge de
jeunesse, l'entrée dans les boites, le petit déjeuner, les guides, etc.

Je connaissais pas Erasmus Travel au départ, j'ai donc raté pas mal de séjours
assez intéressants. Parmi eux : Budapest, Wien, Bratislava, Prague, Berlin,
Amsterdam, Bruxelles.
On voit bien qu'ils vont partout. Au final vous pouvez y aller de vous même,
donc bon, vous ratez juste l'ambiance Erasmus.

Et donc, pour ceux que j'ai _pas_ ratés, j'ai été assez satisfait. Ci-après,
des photos, des prix, des descriptions…

### Auschwitz

![Auschwitz II : Birkenau](images/auschwitz.jpg)

- Prix : 60 zł (15€)

Auschwitz, c'est à même pas 2h de Cracovie. Si vous ne l'avez pas fait, autant
y aller. Pas forcément une grosse teuf comme visite, mais faut voir les camps.
Transport ainsi que guide compris. Le prix d'une visite est de 30 zł en temps
normal, ce qui fait environ 8€ de transport, pas excessif.

### Wieliczka Salt Mine

![Mine de sel de Wieliczka](images/wieliczka.jpg)

* Prix : 50 zł (13€)

Le sud de la Pologne est connu pour ses mines de sel. Dont Wieliczka, la plus
grande du pays !
La visite dure bien 3 heures, la mine est énorme, plus de 300km de galerie.
De mémoire, environ 7km pour la visite.
Le prix d'une visite en anglais est de 79 zł, inutile de dire que vous y gagnez
déjà. À ça on rajoute le transport, ça fait une visite pour que dalle.

Notre guide était marrant, faisait des blagues assez souvent et rendait donc
la visite très intéressante. On était un groupe de 20 personnes, parfait pour
ce genre de choses.
Aussi, vous remarquerez, ils aiment les églises en Pologne. Genre, beaucoup,
vous verrez facilement deux chapelles durant la visite. Une étant pas mal
grande, avec une statue de Jean-Paul II entre autres…

\newpage 

### Vilnius (Lituanie) et Riga (Lettonie)

![Statues à Vilnius](images/vilnius.jpg)

- Prix : 480 zł (120€)

Dans le prix, d'inclus : une nuit à Vilnius avec petit déjeuner, une visite
guidée, deux nuits à Riga avec encore une foit le petit déjeuner et la visite
de la ville. Mais on oublie pas les trois soirées Erasmus mais surtout le bus !
Mine de rien, Riga et Vilnius c'est un peu loin de Cracovie, faut pas avoir
peur des 14h de bus !
Raison de plus pour y aller, c'est encore plus loin de France ! Et puis vous
saurez finalement où est la Lituanie et où est la Lettonie.

Vilnius est une très jolie ville, très blanche par contre. Les bâtiments ont
été reconstruits après la seconde guerre mondiale.

![Place à Riga](images/riga.jpg)

Riga a été ma ville préférée. Beaucoup plus « authentique » que Vilnius. À
comprendre que tout n'a pas un look trop moderne, on s'y sent pas mal !
La vie n'y est pas trop chère non plus. Petit plus : ils ont l'euro, comme en
Lituanie.

N'oubliez surtout pas de goûter le
Balzams\footnote{\url{https://fr.wikipedia.org/wiki/Baume_noir_de_Riga}}, un
alcool à 45° similaire au Jägermeister, en plus amer. Il y a aussi une version
cassis, beaucoup plus sucrée mais à 38° tout de même !
De nombreux magasins proposent des dégustations avant d'en acheter pour se
faire une idée. L'alcool fort à 9h au réveil, ça a presque l'air normal.

\newpage

## Par soi-même

Tous les trajets sont, sauf mention contraire, effectués en bus. Je donne les
prix depuis Cracovie pour la plupart mais vous pouvez très bien, comme moi,
enchaîner plusieurs villes. Ce sont des aller-retours aussi !
J'ai par exemple fait Cracovie → Gdańsk → Varsovie → Tallinn → Helsinki → 
Varsovie → Łódź → Cracovie en deux semaines.

Pour les compagnies, voici les plus intéressantes :

- Polskibus\footnote{\url{http://www.polskibus.com/}} : LE réseau de bus en
Pologne. Avec eux, vous allez presque partout pour rien. Moins de 10€ par
exemple l'aller-retour Varsovie ! Les bus sont grands, confortables et vous
avez du wifi gratos.
- MDA\footnote{\url{http://rozklady.mda.malopolska.pl/}} : tout simplement un
site rencensant les bus enregistrés entre autres à la gare de Cracovie. Vous
trouverez par exemple ici les bus pour Zakopane, ou encore Auschwitz. Vous
pouvez acheter les billets directement à la gare, suffit juste de regarder les
horaires avant de partir.
- Ecolines\footnote{\url{http://ecolines.net/en/}} : pour partir à Riga, 
Vilnius, Tallinn et bien d'autres ! Les bus sont très confortables et équipés 
de tablettes dans les dossiers pour regarder des films ou écouter de la
musique. Bon, c'est en russe et sous-titré anglais, mais on s'y fait, au pire
y'a le wifi.
- StudentAgency\footnote{\url{http://www.studentagencybus.com/}} : Une
compagnie basée en République Tchèque. Vous n'irez donc qu'à Prague avec eux
mais c'est déjà bien ! L'aller se fait en bus jusqu'à Katowice puis en train
jusqu'à Prague si je me souviens bien.

### Zakopane

![Régions des lacs](images/zakopane.jpg)

- Prix : 30 zł (7,5€)

Zakopane, c'est la capitale hivernale de la Pologne. Y'a donc moyen d'y faire
du ski en hiver, même donc à votre arrivée en mars ! Mais ce qui est vraiment
intéressant, ce sont les randonnées, c'est époustouflant…
J'ai eu la flemme d'y aller avec Erasmus donc j'y suis reparti de moi même plus
tard, faut pas manquer les paysages… Le sommet le plus haut doit être à 2400m.

Il y a plusieurs lacs à voir, le plus intéressant étant Morskie Oko à environ
1400m d'altitude. Interdiction de se baigner dans les lacs par contre ! De
toute façon à 10°C je vous confirme que c'est pas bien chaud, même en été.

Pour dormir c'est facile, des chalets bordent les différents lacs et on peut y
dormir pour 40 zł (10€), voire moins si c'est complet et que vous demandez
juste de quoi dormir sur le sol.
En gros, on peut se faire Zakopane - lacs - Zakopane en deux jours avec, allez,
6h de marche par jour. Perso, j'ai fait 3h le premier jour l'aprem et 9h le
lendemain. Plein d'itinéraires existent, on peut même aller en Slovaquie, c'est
à la frontière.

![](images/zakopane2.jpg)

![J'ai tout compris](images/zakopane3.jpg)

\newpage

### Ogrodzieniec

![Château d'Ogrodzieniec](images/ogrodzieniec.jpg)

- Prix : 25 zł (6€)

Situé à 60km de Cracovie, entre Katowice et Częstochowa, ce serait con de rater
ce château ! L'entrée coûte que dalle, de l'ordre de 5€ environ.

Il faut bien une heure ou deux pour le visiter. Les panneaux sont traduits en
anglais, assez pratique.

\newpage

### Gdańsk

![Quais de Gdańsk](images/gdansk.jpg)

- Prix : 130 zł (32€)

Vous connaissez peut-être Gdańsk sous le nom de Danzig, ville tout au nord de
la Pologne, donnant sur la mer.
Géniale avec du beau temps, on peut y boire une bonne bière en regardant les
vieux bâteaux passer. Il est également possible de monter sur la cathédrale,
offrant ainsi une bonne vue sur la ville.

Gdańsk fait partie de la Tricité, trois villes l'une à côté de l'autre.
Vous pourrez ainsi aller à Gdynia, la ville industrielle et Sopot, la 
balnéaire.

Chaque année, c'est le festival Open'er\footnote{\url{http://opener.pl/}} qui
se produit à Gdynia. Pas mal grand et plutôt prisé !

\newpage

### Sopot

![Boire une bière sur la plage](images/sopot.jpg)

- Prix : Pas cher depuis Gdańsk

Sopot est donc à une heure en train de Gdańsk. La ville en elle même est assez
touristique. Mais ça fait toujours plaisir d'acheter une petite glace avant
d'aller boire une bière sur la plage.
Surtout quand il fait encore jour vers 22h, on a bien l'impression d'être en
vacances !

\newpage

### Malbork

![Forteresse teutonique de Malbork](images/malbork.jpg)

- Prix : Pas cher depuis Gdańsk

Située à une heure de train de Gdańsk, avec des connexions très fréquentes,
faut y aller. C'est une forteresse énorme composée de trois châteaux imbriqués,
construire au 13e siècle.

La visite se fait avec un audio-guide, dispo en français, et dure dans les
trois heures si vous voulez tout faire. Assez intéressant, vous avez aussi des
magasins vendant de l'ambre et autres trucs dans les cours. À rappeler que le
nord de la Pologne c'est le pays de l'ambre.

Il y a deux restaurants dans le château, et j'ai été étonné du prix ! Un bon
repas et deux bières pour même pas 12€.

\newpage

### Varsovie

![Rynek de Varsovie](images/varsovie.jpg)

- Prix : 30 zł (7,5€)

La ville est grande, bien plus que Cracovie, et ça se voit. Ils ont même un
métro, climatisé qui plus est, prends ça Paris. Vous pouvez donc vous balader
dans la vieille ville et la nouvelle ville, qui est en fait elle aussi, 
vieille. Et à côté de ça prendre le métro, le bus, le train pour vous rendre à
peu près n'importe où.

En trucs sympas à voir il y a donc la vieille ville ainsi que la nouvelle, qui 
était une ville indépendante adjacente à Varsovie à l'époque. Plusieurs statues
de Marie Curie sont présentes, faut pas oublier qu'elle était polonaise !
Varsovie possède comme Cracovie sa _Barbakan_, petite fortification sympa.

Puis bah c'est la Pologne, une petite bière sur le rynek, une glace s'il fait
chaud et c'est pépère !

\newpage

### Tallinn (Estonie)

![Église St-Nicolas](images/tallinn.jpg)

- Prix : 70€ depuis Varsovie

J'ai passé quatre jours à Tallinn, hébergé gracieusement par une amie y faisant
son stage (cassedédi à toi, plein d'amour).
Une ville assez petite, où il ne fait pas vraiment nuit en été, et pleine de
charme !
Niveau prix, c'est comme en Pologne : que dalle. Mais bonus, c'est en euro !

L'aspect médiéval y est encore bien présent avec de petites allées, les tours
de garde et chemins de ronde assez imposants.

Truc fou en Estonie : les gens ont un sauna dans leur appartement ! Parfait
pour tomber malade si vous ne faites que ça de votre soirée, surtout si sur le
balcon ça caille.


\newpage

### Helsinki (Finlande)

![Place du sénat](images/helsinki.jpg)

- Prix : 40€ depuis Tallinn

Helsinki, c'est super rèche, vous attardez pas trop. À compendre : allez-y le
matin et revenez le soir, vous avez des ferries assez souvent et ça coûte moins
cher de prendre un aller-retour dans la journée.
Manque de bol j'y suis allé quand il pleuvait, mais ça doit être sympa quand il
fait beau tout de même.

À part ça, c'est un pays intéressant, tout le monde y parle anglais et très
bien, même les vieux, assez impressionant ! Allez essayer en France…

N'oubliez pas de visiter l'île fortifiée Suomenlinna, seulement 5€ depuis le
centre en petit bâteau.

\newpage

### Wrocław

![Place principale](images/wroclaw.jpg)

- Prix : 56 zł (14€)

À environ 3h de bagnole de Cracovie, c'est presque à côté on va dire, à 
l'ouest, après Katowice.

La place est assez grande, les maisons très jolies ! Il est possible de monter
sur les tours de deux églises, offrant une magnifique vue.
Après on peut toujours boire une bière à Bierhalle, un petit restaurant de
style allemand, même avec les serveuses !

Payer 15€ la nuit par personne dans un appart énorme à 4, pile dans le centre,
trouvez moi mieux.

Wrocław a aussi un zoo sympa, le genre où on se balade dans des tunnels en
verre dans l'aquarium.

\newpage

### Dresden (Allemagne)

![Grande place avec plein de trucs](images/dresden.jpg)

- Prix : j'y suis passé en rentrant en voiture

On en revient pas tellement c'est beau Dresden ! Plusieurs bâtiments on été
reconstruits, faute à la guerre, on se retrouve donc avec certains un peu
noirâtre mais ça passe.
N'oubliez surtout pas d'aller au Zwinger, ancien lieu des festivités des rois.
En somme, un énorme palais avec une cour magnifique, des fontaines, des
statues…

Une bonne schnitzel avec une Paulaner, ça devrait en contenter plus d'un aussi.
Le top reste quand même de se poser dans un biergarten au bord de l'Elbe pour
boire une bière avec vue sur les monuments !

\newpage

### Berlin (Allemagne)

![Cathédrale de Berlin](images/berlin.jpg)

- Prix : pareil que Dresden

Bon euh… Berlin c'est fat, je sais même pas par quoi commencer, ville 8 fois
plus large que Paris… Il y a une tonne de musées sur tout et n'importe quoi.

Pour sûr, allez voir le mur, ça c'est obligé. Peut-être Checkpoint Charlie
aussi, y'a des animations. La Fernsehturm offre une belle vue sur la ville.
Juste à côté il y a le Berliner Dom : la cathédrale.

Posez vous dans un Biergarten ou dans un des nombreux parcs, ramenez vos
bières, sentez les gens qui dansent et qui kiffent.

\newpage

# Lexique

En polonais, on roule les r ! La prononciation est approximative, allez sur
le wiktionnaire si jamais vous doutez.\
Pour les nombres, regardez sur ce
site\footnote{\url{https://fr.wikiversity.org/wiki/Polonais/Vocabulaire/Nombres}}.

\vspace{1cm}

\begin{tabularx}{13.2cm}{|l|l|X|}
    \hline
    \textbf{Français} & \textbf{Polonais} & \textbf{Prononciation} \\
    \hline
    Bonjour & Dzień dobry & djegn dobré \\
    \hline
    Salut & Cześć & Tchechtch \\
    \hline
    Au revoir & Do widzenia & — \\
    \hline
    À plus & Na razie & na razié \\
    \hline
    Merci & Dziękuję & djènekouyé \\
    \hline
    S'il vous plaît & Proszę & procheu \\
    \hline
    Oui & Tak & — \\
    \hline
    Non & Nie & Nié \\
    \hline
    Bière & Piwo & Pivo \\
    \hline
    Vodka & Wódka & Voudka \\
    \hline
    Pardon & Przepraszam & Pchéprachame \\
    \hline
    Je suis français & Jestem francuzem & Yésteum frantsuzème \\
    \hline
    Je parle pas polonais & Nie mówię po polsku & nié mouvié po polskou \\
    \hline
    Je ne comprends pas & Nie rozumiem & Nié rozoumième \\
    \hline
    SVP une bière & Poproszę piwo & poprocheu pivo \\
    \hline
    Je m'appelle Maël & Mam na imię Maël & Mame na imié Maël \\
    \hline
    Ici & tutaj & toutaye \\
    \hline
    Là-bas & tam & tame \\
    \hline
    Combien & ile & ilé \\
    \hline
    Où est la gare ? & Gdzie jest dworzec? & Gdzié yest dvojetz \\
    \hline

\end{tabularx}

\newpage

# Conclusion

Je sais pas trop quoi raconter dans la conclusion… Si vous avez lu le rapport,
ou du moins une bonne partie, vous devriez pouvoir vous faire une bonne idée
de la Pologne.

Mais comme on sait jamais : la Pologne ça peut paraître un peu austère au
premier abord mais dès qu'on y vit, c'est très différent. Cracovie est très
colorée, les gens sont cools, on y passe du bon temps. D'autant plus que la vie
ne coûte pas trop cher là-bas ! De nombreux événements prennent place dans
l'ex-capitale de la Pologne, pas de quoi s'ennuyer. La ville est très appréciée
de la population, j'oserais même dire que c'est la plus belle ville du pays.

Et j'insisterai jamais assez là-dessus : voyagez. Avec presque 5 mois de
vacances ça serait con de passer à côté de tout ce que l'Est a à offrir ! Pour
rien que quelques złoty vous pouvez aller à peu près où vous voulez. Il y a des
villes que j'ai pas marquées mais ça vaut le coup par exemple d'aller à 
Bochnia, ils ont aussi une mine de sel intéressante. Ou sinon vous allez à la
gare et vous prenez le prochain bus, pour 4€ aller retour vous allez visiter
une ville ou un village au pif, ça vaut le coup !

Votre semestre à l'international, ce n'est pas que pour les cours. C'est aussi
pour découvrir une autre culture, une autre langue, d'autres modes de vie.
Repartir en ayant une idée totalement différente du pays, c'est ce qu'il faut
faire.

Go Pologne.
